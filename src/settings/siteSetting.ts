// github repo url
export const GITHUB_URL = "https://github.com/battcn/wemirr-platform-ui";

// vue-skill-full-next-doc
export const DOC_URL = "https://github.com/battcn/wemirr-platform-ui";

// site url
export const SITE_URL = "https://cloud.battcn.com";
